﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using api_Segimob.Models;

namespace api_Segimob.Controllers
{
    public class VendaProdutosController : ApiController
    {
        private SegimobModel db = new SegimobModel();

        // GET: api/VendaProdutos
        public IEnumerable<VendaProduto> GetVendaProdutos()
        {
            return db.VendaProdutos.ToList();
        }

        // GET: api/VendaProdutos/5
        [ResponseType(typeof(VendaProduto))]
        public IHttpActionResult GetVendaProduto(long id)
        {
            VendaProduto vendaProduto = db.VendaProdutos.Find(id);
            if (vendaProduto == null)
            {
                return NotFound();
            }

            return Ok(vendaProduto);
        }

        // PUT: api/VendaProdutos/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVendaProduto(long id, VendaProduto vendaProduto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vendaProduto.id)
            {
                return BadRequest();
            }

            db.Entry(vendaProduto).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VendaProdutoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/VendaProdutos
        [ResponseType(typeof(VendaProduto))]
        public IHttpActionResult PostVendaProduto(VendaProduto vendaProduto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.VendaProdutos.Add(vendaProduto);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = vendaProduto.id }, vendaProduto);
        }

        // DELETE: api/VendaProdutos/5
        [ResponseType(typeof(VendaProduto))]
        public IHttpActionResult DeleteVendaProduto(long id)
        {
            VendaProduto vendaProduto = db.VendaProdutos.Find(id);
            if (vendaProduto == null)
            {
                return NotFound();
            }

            db.VendaProdutos.Remove(vendaProduto);
            db.SaveChanges();

            return Ok(vendaProduto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VendaProdutoExists(long id)
        {
            return db.VendaProdutos.Count(e => e.id == id) > 0;
        }
    }
}
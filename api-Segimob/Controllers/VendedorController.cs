﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using api_Segimob.Models;

namespace api_Segimob.Controllers
{
    public class VendedorController : ApiController
    {
        private SegimobModel db = new SegimobModel();

        // GET: api/Vendedor
        public IEnumerable<Vendedor> GetVendedors()
        {
            return db.Vendedors.ToList();
        }

        // GET: api/Vendedor/5
        [ResponseType(typeof(Vendedor))]
        public IHttpActionResult GetVendedor(int id)
        {
            Vendedor vendedor = db.Vendedors.Find(id);
            if (vendedor == null)
            {
                return NotFound();
            }

            return Ok(vendedor);
        }

        // PUT: api/Vendedor/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVendedor(int id, Vendedor vendedor)
        {
            if (id != vendedor.id)
            {
                return BadRequest();
            }

            db.Entry(vendedor).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VendedorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Vendedor
        [ResponseType(typeof(Vendedor))]
        public IHttpActionResult PostVendedor(Vendedor vendedor)
        {
            db.Vendedors.Add(vendedor);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = vendedor.id }, vendedor);
        }

        // DELETE: api/Vendedor/5
        [ResponseType(typeof(Vendedor))]
        public IHttpActionResult DeleteVendedor(int id)
        {
            Vendedor vendedor = db.Vendedors.Find(id);
            if (vendedor == null)
            {
                return NotFound();
            }

            db.Vendedors.Remove(vendedor);
            db.SaveChanges();

            return Ok(vendedor);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VendedorExists(int id)
        {
            return db.Vendedors.Count(e => e.id == id) > 0;
        }
    }
}
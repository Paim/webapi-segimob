﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using api_Segimob.Models;

namespace api_Segimob.Controllers
{
    public class VendaController : ApiController
    {
        private SegimobModel db = new SegimobModel();

        // GET: api/Venda
        public IEnumerable<Venda> GetVendas()
        {
            db.Configuration.LazyLoadingEnabled = true;
            return db.Vendas.ToList();
        }

        // GET: api/Venda/5
        [ResponseType(typeof(Venda))]
        public IHttpActionResult GetVenda(long id)
        {
            Venda venda = db.Vendas.Find(id);
            if (venda == null)
            {
                return NotFound();
            }

            return Ok(venda);
        }
        
        // PUT: api/Venda/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVenda(long id, Venda venda)
        {
            if (id != venda.id)
            {
                return BadRequest();
            }

            db.Entry(venda).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VendaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Venda
        [ResponseType(typeof(Venda))]
        public IHttpActionResult PostVenda(Venda venda)
        {
            db.Vendas.Add(venda);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = venda.id }, venda);
        }

        // DELETE: api/Venda/5
        [ResponseType(typeof(Venda))]
        public IHttpActionResult DeleteVenda(long id)
        {
            Venda venda = db.Vendas.Find(id);
            if (venda == null)
            {
                return NotFound();
            }

            db.Vendas.Remove(venda);
            db.SaveChanges();

            return Ok(venda);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VendaExists(long id)
        {
            return db.Vendas.Count(e => e.id == id) > 0;
        }
    }
}